FROM scratch
EXPOSE 8080
ENTRYPOINT ["/stage"]
COPY ./bin/ /